--liquibase formatted sql
--changeset microservice_class:20230718_1
INSERT INTO daily_transactions
(id, transaction_id, from_cif, beneficiary, amount, product_id, product_code, status, created_at, saga_id)
VALUES ('6364bf5d-fa89-4ab3-b274-1da8cdb785b3', '1e9b8002-690a-4083-bd4b-9ef2141ced98', 'user_1', 'user_5', 10,
        'a51fea9c-ea7a-4a02-bfd4-0eec3aa55a5a', 'INTERNAL_TRANSFER', 'RESERVED', '2023-07-18 11:34:53.629', uuid_generate_v4()), --duplicated
       ('66620d36-6a37-4ce3-9db0-783ee3fd11b4', 'e2056e67-ca63-4cd3-a108-0dee11b2a88a', 'user_1', 'user_5', 10,
        'a51fea9c-ea7a-4a02-bfd4-0eec3aa55a5a', 'INTERNAL_TRANSFER', 'RESERVED', '2023-07-18 11:34:55.015', uuid_generate_v4()),
       ('c935320b-3d10-43e0-b1a2-885c1ad43141', '0119e388-8d2e-460a-84e8-d96e86b6fe60', 'user_1', 'user_5', 10,
        'a51fea9c-ea7a-4a02-bfd4-0eec3aa55a5a', 'INTERNAL_TRANSFER', 'RESERVED', '2023-07-18 11:34:55.819', uuid_generate_v4()),
       ('b355be82-fab8-41da-9a90-cd091ca9fd70', 'c35ef116-acdf-44fe-8f8f-74e3936dfe18', 'user_1', 'user_5', 10,
        'a51fea9c-ea7a-4a02-bfd4-0eec3aa55a5a', 'INTERNAL_TRANSFER', 'RESERVED', '2023-07-18 11:34:56.089', uuid_generate_v4()),
       ('ac0c1c7d-fc42-4e15-9504-1f736053f218', '3c26fd3e-5eb4-4b25-873f-e9eb3f6cdbc6', 'user_1', 'user_5', 10,
        'a51fea9c-ea7a-4a02-bfd4-0eec3aa55a5a', 'INTERNAL_TRANSFER', 'RESERVED', '2023-07-18 11:34:56.344', uuid_generate_v4()),
       ('77c631ab-ed3f-45cb-a9cd-37ba3a9a2114', '75a60ace-0243-46f8-88c8-94c111fe2ba3', 'user_1', 'user_5', 10,
        'a51fea9c-ea7a-4a02-bfd4-0eec3aa55a5a', 'INTERNAL_TRANSFER', 'RESERVED', '2023-07-18 11:34:56.613', uuid_generate_v4()),
       ('8e99a05a-ce35-4d8b-b1dd-9ebe81fd37de', 'c4fa5a7f-ceea-4e98-b76f-c785cb2b2794', 'user_1', 'user_5', 10,
        'a51fea9c-ea7a-4a02-bfd4-0eec3aa55a5a', 'INTERNAL_TRANSFER', 'RESERVED', '2023-07-18 11:34:56.864', uuid_generate_v4()),
       ('776c8c09-e5f2-474b-b46b-7d4d08dcd31f', '9ee1229b-f73c-491f-a77a-d8501fbb9973', 'user_1', 'user_5', 10,
        'a51fea9c-ea7a-4a02-bfd4-0eec3aa55a5a', 'INTERNAL_TRANSFER', 'RESERVED', '2023-07-18 11:34:57.596', uuid_generate_v4()),
       ('ae6e30f6-e2ff-469a-ab5b-9e4230042dea', '3faf45d4-b7df-4004-843b-82033c0fee9f', 'user_1', 'user_5', 10,
        'a51fea9c-ea7a-4a02-bfd4-0eec3aa55a5a', 'INTERNAL_TRANSFER', 'RESERVED', '2023-07-18 11:34:58.144', uuid_generate_v4()),
       ('4b956c39-f98a-46a4-96fb-889a0d2a54db', '84e1fa94-06fd-4c56-bc95-29be2545812e', 'user_1', 'user_5', 10,
        'a51fea9c-ea7a-4a02-bfd4-0eec3aa55a5a', 'INTERNAL_TRANSFER', 'RESERVED', '2023-07-18 11:34:58.555', uuid_generate_v4());
INSERT INTO daily_transactions (id, transaction_id, from_cif, beneficiary, amount, product_id, product_code, status,
                                created_at, saga_id)
VALUES ('58fb98a0-1143-4665-9bce-ec5071224f71', 'e7d20c53-fc9f-4130-b992-4695ee619c4e', 'user_1', 'user_5', 10, --reverted
        'a51fea9c-ea7a-4a02-bfd4-0eec3aa55a5a', 'INTERNAL_TRANSFER', 'RESERVED', '2023-07-18 11:34:58.780', uuid_generate_v4()),
       ('9ddc32c7-584a-4790-9f84-94e7e89fa1e5', 'd5e826a7-d5fb-443a-86f0-7cc59342d2bc', 'user_1', 'user_5', 10,
        'a51fea9c-ea7a-4a02-bfd4-0eec3aa55a5a', 'INTERNAL_TRANSFER', 'RESERVED', '2023-07-18 11:34:59.077', uuid_generate_v4()),
       ('61b59a98-15ad-4aaf-8d01-60db8be17e39', 'd6105f51-b35c-4f0c-878a-fa504b8192f2', 'user_1', 'user_5', 10,
        'a51fea9c-ea7a-4a02-bfd4-0eec3aa55a5a', 'INTERNAL_TRANSFER', 'RESERVED', '2023-07-18 11:34:59.341', uuid_generate_v4()),
       ('2004fbe0-d703-42b7-937d-e55ffbdc217c', '4519601a-3779-4d9f-8617-5f159b7bbf7f', 'user_1', 'user_5', 10,
        'a51fea9c-ea7a-4a02-bfd4-0eec3aa55a5a', 'INTERNAL_TRANSFER', 'RESERVED', '2023-07-18 11:34:59.590', uuid_generate_v4()),
       ('3e0a95c2-e501-467d-a4b9-2d19b702ecc4', '0a009632-1f28-496f-9a85-9f102f88edc5', 'user_1', 'user_5', 10,
        'a51fea9c-ea7a-4a02-bfd4-0eec3aa55a5a', 'INTERNAL_TRANSFER', 'RESERVED', '2023-07-18 11:34:59.842', uuid_generate_v4()),
       ('4d4f728b-5412-44f5-b803-a1a7952ec6a7', '23c3b312-ba4b-484d-b042-cc5662191f19', 'user_1', 'user_5', 10,
        'a51fea9c-ea7a-4a02-bfd4-0eec3aa55a5a', 'INTERNAL_TRANSFER', 'RESERVED', '2023-07-18 11:35:00.196', uuid_generate_v4()),
       ('c07a23d7-e4b8-4caa-b244-8b8f92f1463c', 'a3448b5a-0f3c-4cb9-a8ff-f54104c7f936', 'user_1', 'user_5', 10,
        'a51fea9c-ea7a-4a02-bfd4-0eec3aa55a5a', 'INTERNAL_TRANSFER', 'RESERVED', '2023-07-18 11:35:00.481', uuid_generate_v4()),
       ('1a758ea7-efdc-46f5-9b67-a5258e3674d4', 'bd74772e-cb20-483b-8676-5003ac0c1bb7', 'user_1', 'user_5', 10,
        'a51fea9c-ea7a-4a02-bfd4-0eec3aa55a5a', 'INTERNAL_TRANSFER', 'RESERVED', '2023-07-18 11:35:00.837', uuid_generate_v4()),
       ('30824d5e-daab-4a76-854f-0b3499f50cf7', '0c66eaa4-dbca-48f7-a946-6d310daf0951', 'user_1', 'user_5', 10,
        'a51fea9c-ea7a-4a02-bfd4-0eec3aa55a5a', 'INTERNAL_TRANSFER', 'RESERVED', '2023-07-18 12:00:03.033', uuid_generate_v4()),
       ('5dd4e895-adb5-40ad-9e26-7f885efaa5bd', 'bbfe454d-0c2b-46b4-8499-76de88a5c7a7', 'user_1', 'user_5', 10,
        'a51fea9c-ea7a-4a02-bfd4-0eec3aa55a5a', 'INTERNAL_TRANSFER', 'RESERVED', '2023-07-18 12:00:03.371', uuid_generate_v4());


-- duplicated transaction
INSERT INTO daily_transactions (id, transaction_id, from_cif, beneficiary, amount, product_id, product_code, status,
                                created_at, saga_id)
VALUES ('ea6bb9ca-88ff-4554-a89f-262f711395e7', '1e9b8002-690a-4083-bd4b-9ef2141ced98', 'user_1', 'user_5', 10,
        'a51fea9c-ea7a-4a02-bfd4-0eec3aa55a5a', 'INTERNAL_TRANSFER', 'RESERVED', '2023-07-18 11:35:53.629', uuid_generate_v4());
-- revert limit transaction
INSERT INTO daily_transactions (id, transaction_id, from_cif, beneficiary, amount, product_id, product_code, status,
                                created_at, saga_id)
VALUES ('08628867-9b66-4a03-9c57-d5f36f51b896', 'e7d20c53-fc9f-4130-b992-4695ee619c4e', 'user_1', 'user_5', 10,
        'a51fea9c-ea7a-4a02-bfd4-0eec3aa55a5a', 'INTERNAL_TRANSFER', 'REVERTED', '2023-07-18 11:34:58.780', uuid_generate_v4());