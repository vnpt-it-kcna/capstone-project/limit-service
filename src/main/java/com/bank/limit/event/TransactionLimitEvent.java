package com.bank.limit.event;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.UUID;

@Getter
@Setter
public class TransactionLimitEvent {
    private String sagaId;
    private UUID clientTransactionId;
    private Instant checkedTime;
    private BigDecimal accumulatedDailyAmount;
    private BigDecimal accumulatedMonthlyAmount;
}
