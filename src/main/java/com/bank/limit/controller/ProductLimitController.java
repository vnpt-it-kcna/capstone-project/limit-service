package com.bank.limit.controller;

import com.bank.limit.model.UserLimitRequest;
import com.bank.limit.service.ProductEventService;
import com.bank.limit.service.ProductLimitConfigService;
import com.bank.limit.service.UserLimitService;
import com.netflix.discovery.converters.Auto;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/restapi")
public class ProductLimitController {
    @Autowired
    private ProductLimitConfigService productLimitConfigService;
    @Autowired
    private ProductEventService productEventService;
    @Autowired
    private UserLimitService userLimitService;

    @PostMapping(value = "/v1/user-lmit", produces = "application/json")
    @Operation(summary = "Retrieve user limit")
    public ResponseEntity<?> updateProductLimit(
            @Valid @RequestBody UserLimitRequest userLimitRequest) {
        return ResponseEntity.ok(userLimitService.retrieveUserLimitTxn(userLimitRequest));
    }

    @GetMapping(value = "/v1/product-limits", produces = "application/json")
    @Operation(summary = "Retrieve All Product Limits")
    public ResponseEntity<?> retrieveProductLimits() {
        return ResponseEntity.ok(productLimitConfigService.retrieveProductLimits());
    }

    @GetMapping(value = "/v1/product-limit-events", produces = "application/json")
    @Operation(summary = "Retrieve All Product Limits")
    public ResponseEntity<?> retrieveProductLimitEvents() {
        return ResponseEntity.ok(productEventService.retrieveProductLimitEvents());
    }
}
